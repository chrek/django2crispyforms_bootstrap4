"""
Defintion for User and Sponsor views.
"""
from django.views.generic import ListView, CreateView, UpdateView
from records.models import User, Sponsor
from records.forms import SponsorForm

from django.urls import reverse_lazy

# Create your views here.
class SponsorListView(ListView):
    model = Sponsor
    context_object_name = 'records'

class UserCreateView(CreateView):
    """
    This defines the view to use the User model.
    """
    model = User
    template_name = 'login.html'
    fields = ('name', 'email', 'password')

class SponsorCreateView(CreateView):
    """
    This defines the view to use the Sponsor model.
    """
    model = Sponsor
    fields = ('name', 'email', 'department', 'role', 'job_description')
    success_url = reverse_lazy('sponsor_list')

class SponsorUpdateView(UpdateView):
    """
    This defines the view to use the Sponsor model for update.
    """
    model = Sponsor
    form_class = SponsorForm
    template_name = 'records/sponsor_update_form.html'
    success_url = reverse_lazy('sponsor_list')
