"""
Defintion for User and Sponsor models.
"""
from django.db import models

# Create your models here.

class User(models.Model):
    """
    This defines the User model for the app.
    """
    name = models.CharField(max_length=120)
    email = models.EmailField(blank=True)
    password = models.CharField(max_length=60)

class Sponsor(models.Model):
    """
    This defines the Sponsor model for the app.
    """
    name = models.CharField(max_length=120)
    email = models.EmailField(blank=True)
    department = models.CharField(max_length=60)    
    role = models.CharField(max_length=30, blank=True)    
    job_description = models.TextField(blank=True)