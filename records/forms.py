"""
Defintion for User and Sponsor models.
"""
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from records.models import Sponsor
# Create your models here.

class SponsorForm(forms.ModelForm):
    """
    This defines the Sponsor model for the app.
    """
    class Meta:
        model = Sponsor
        fields = ('name', 'email', 'department', 'role', 'job_description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save sponsor'))
