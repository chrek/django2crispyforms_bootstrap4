# django2crispyforms_bootstrap4

This Django project has one application.
There are 4 pages using the django-crispy-forms Django application with Bootstrap 4.
And all the pages share a base template.

1. The login page uses the basic crispy form styling with {% crispy %} tag.
2. The other pages are using the as_crispy_field template filter to
    rendering fields.

----
## Django Crispy Forms

Django-crispy-forms provides you with a `|crispy filter` and `{% crispy %}` tag 
that will let you control the rendering behavior of your Django forms in a 
very elegant and DRY way. Have full control without writing custom form templates. All this without breaking the standard way of doing things in Django, so it plays nice with any other form application.
----

The main features that have currently been implemented are:

* Render Django's basic form using Bootstrap 4 CSS classes
* Used  |crispy filter and {% crispy %} tag

----
  
Start the Django dev server with `py manage.py runserver` 

## Access the website

- Access the login page from the browser using `http://127.0.0.1:8000/login`
- Access to add a Sponsor using `http://127.0.0.1:8000/add/`
- Access to edit a Sponsor (id=1) using `http://127.0.0.1:8000/1/edit/`
- Access to display available Sponsors using `http://127.0.0.1:8000/`

## References

1.	[simpleisbetterthancomplex](https://simpleisbetterthancomplex.com/tutorial/2018/08/13/how-to-use-bootstrap-4-forms-with-django.html)
2.  [readthedocs](https://django-crispy-forms.readthedocs.io/en/latest/)
3.  [techiediaries](https://www.techiediaries.com/django-form-bootstrap/)

